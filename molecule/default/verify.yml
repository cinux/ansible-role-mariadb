---
# This is an example playbook to execute inspec tests.
# Tests need distributed to the appropriate ansible host/groups
# prior to execution by `inspec exec`.
- name: Verify
  hosts: all
  become: true
  vars:
    inspec_bin: /opt/inspec/bin/inspec
    inspec_download_source_dir: /usr/local/src
    inspec_test_directory: /tmp/molecule/inspec
  tasks:
    - name: Setting variables (CentOS 6 / RHEL 6 / Amazon Linux 2018)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/3.9.3/el/6/inspec-3.9.3-1.el6.x86_64.rpm"
        inspec_download_sha256sum: 36c8605f3c1a85acd4e2d54ab01b2737832af41126e307fece7989c6332ae534
      when: (ansible_facts['os_family'] == "RedHat" and (ansible_facts['distribution_major_version'] == "6" or ansible_facts['distribution_major_version'] == "2018"))
    - name: Setting variables (CentOS 7 / RHEL 7 / Amazon Linux 2)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/3.9.3/el/7/inspec-3.9.3-1.el7.x86_64.rpm"
        inspec_download_sha256sum: 4d54d12899c2eeaae4812cd13b8dfcae01ec1fd4a44f00cab77e31a57aea502b
      when: (ansible_facts['os_family'] == "RedHat" and (ansible_facts['distribution_major_version'] == "7" or ansible_facts['distribution_major_version'] == "2"))
    - name: Setting variables (RHEL 9)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/4.3.2/el/7/inspec-4.3.2-1.el7.x86_64.rpm"
        inspec_download_sha256sum: 37151ff63cdbc8522f5ecd6a7d9c8bf7ca8fc68b9aad7000a40c1c9d8ba5c9b2
      when: (ansible_facts['os_family'] == "RedHat" and (ansible_facts['distribution_major_version'] == "9" or ansible_facts['distribution_major_version'] == "2"))
    - name: Setting variables (Debian 8 / Ubuntu 14.04)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/3.9.3/ubuntu/14.04/inspec_3.9.3-1_amd64.deb"
        inspec_download_sha256sum: fe29d35fb95916b15310dc58787e63f22c4524be36a5c181307e08090cb0877d
      when: (ansible_facts['os_family'] == "Debian" and (ansible_facts['distribution_major_version'] == "8" or ansible_facts['distribution_major_version'] == "14"))
    - name: Setting variables (Debian 8 / Ubuntu 16.04)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/3.9.3/ubuntu/16.04/inspec_3.9.3-1_amd64.deb"
        inspec_download_sha256sum: 757dd2366a3932adc5fcc9382b30d77de6cc33152585f4c9f94f8918d9d349a7
      when: (ansible_facts['os_family'] == "Debian" and (ansible_facts['distribution_major_version'] == "8" or ansible_facts['distribution_major_version'] == "16"))
    - name: Setting variables (Debian 9 / Ubuntu 18.04)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/3.9.3/ubuntu/18.04/inspec_3.9.3-1_amd64.deb"
        inspec_download_sha256sum: 757dd2366a3932adc5fcc9382b30d77de6cc33152585f4c9f94f8918d9d349a7
      when: (ansible_facts['os_family'] == "Debian" and (ansible_facts['distribution_major_version'] == "9" or ansible_facts['distribution_major_version'] == "18"))
    - name: Setting variables (Debian 11)
      set_fact:
        inspec_download_url: "https://packages.chef.io/files/stable/inspec/5.17.4/debian/11/inspec_5.17.4-1_amd64.deb"
        inspec_download_sha256sum: bc10560bb746e6fdc96f2c6a5126e835c55008bd9af7d13f9320526f49ec90e5
      when: (ansible_facts['os_family'] == "Debian" and (ansible_facts['distribution_major_version'] == "11" or ansible_facts['distribution_major_version'] == "18"))
    - name: Download Inspec
      get_url:
        url: "{{ inspec_download_url }}"
        dest: "{{ inspec_download_source_dir }}"
        checksum: "sha256:{{ inspec_download_sha256sum }}"
        mode: 0755
    - name: Install Inspec (apt)
      apt:
        deb: "{{ inspec_download_source_dir }}/{{ inspec_download_url.split('/')[-1] }}"
        state: present
      when: ansible_pkg_mgr == "apt"
    - name: Install Inspec (yum)
      yum:
        name: "{{ inspec_download_source_dir }}/{{ inspec_download_url.split('/')[-1] }}"
        state: present
      when: ansible_pkg_mgr == "yum"
    - name: Install Inspec (yum)
      dnf:
        name: "{{ inspec_download_source_dir }}/{{ inspec_download_url.split('/')[-1] }}"
        state: present
        disable_gpg_check: true
      when: ansible_pkg_mgr == "dnf"
    - name: Create inspec test directory
      file:
        path: "{{ inspec_test_directory }}"
        state: directory
    - name: Copy inspec test directories
      file:
        path: "{{ inspec_test_directory }}/{{ item.path }}"
        state: directory
        mode: 0755
      with_filetree: "{{ lookup('env', 'MOLECULE_VERIFIER_TEST_DIRECTORY') }}/"
      when: item.state == 'directory'
    - name: Copy inspec test files
      copy:
        src: "{{ item.src }}"
        dest: "{{ inspec_test_directory }}/{{ item.path }}"
        mode: 0644
      with_filetree: "{{ lookup('env', 'MOLECULE_VERIFIER_TEST_DIRECTORY') }}/"
      when: item.state == 'file'
    - name: Accept inspec license
      command: "{{ inspec_bin }} --chef-license=accept-silent"
    - name: Execute Inspec tests
      shell: "{{ inspec_bin }} exec --no-color {{ inspec_test_directory }}"
      register: test_results
      ignore_errors: true
    - name: Display details about the Inspec results
      debug:
        msg: "{{ test_results.stdout_lines }}"
    - name: Fail when tests fail
      fail:
        msg: "Inspec failed to validate"
      when: test_results.rc != 0
