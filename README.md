MariaDB
=========

Install and configure MariaDB.

Requirements
------------

To interact with the Database itself (for example to remove anynoymous user) you need to have a python mysql module installed. Otherwise you will receive following error:

```bash
A MySQL module is required: for Python 2.7 either PyMySQL, or MySQL-python, or for Python 3.X mysqlclient or PyMySQL. Consider setting ansible_python_interpreter to use the intended Python version.
```
For Debian the package is called `python3-mysqldb` and will be installed automaticaly while this role is executed.


Role Variables
--------------

```yml
cinux_mariadb_root_password: Password for the administrator account
```

Dependencies
------------

none

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: cinux.mariadb }

License
-------

MIT
